# Configuração de ambiente MAC para React Native - NEXTI

---

## Instalando Visual Studio Code

### Baixe a opção MacOS Intel Chip (Hackintosh)

Link: https://code.visualstudio.com/download#

---

## Instalando XCode

- Selecione o menu Apple 🍎️ (no canto superior esquerdo da sua tela) > Preferências do Sistema e clique em Iniciar Sessão.

- Clique em “Criar ID Apple” e siga as instruções na tela. OBS: cadastre-se com o email @nexti.com que foi lhe informado.

- Abra a App Store no seu mac.

- Procure por Xcode

- Clique em instalar

- Aguarde a instalação terminar

---

## Instalando Homebrew:

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

## Instalando Node 14 (LTS) e Watchman

```
brew install node@14 watchman
```

## Editando zshrc

```
nano ~/.zshrc

### Cole ao final do arquivo esse seguinte código:

### Enviroment Variables
export PATH=$PATH:/usr/local/opt/node@14/bin
```

## Após a instalação, verifique se ela foi realizada com sucesso com os comandos

```
node -v
npm -v
```

## Instalando Yarn

```
sudo npm install --global yarn
```

## Após a instalação, verifique se ela foi realizada com sucesso com o comando:

```
yarn -v
```

---

## Instalando JDK 11

Link: https://www.oracle.com/java/technologies/javase-jdk11-downloads.html

### Escolha a opção macOS Installer. Para realizar o download, é preciso se cadastrar no site (gratuitamente). Após o download terminar, execute o arquivo .dmg e instale seguindo os passos do instalador.

---

## Preparativos Android Studio

### Criando uma pasta em um local desejado para instalação da SDK.

- Abra um novo terminal e digite os seguintes comandos:

  - mkdir Android

  - cd Android

  - open .

  - com o finder aberto no diretório 'Android', crie uma pasta com o nome: 'Sdk'

- Anote também o endereço de instalação do JDK 11. Se você tiver o XCode instalado, execute esse comando:

```
/usr/libexec/java_home

E ele irá retornar o caminho completo. Guarde esse caminho.
```

## Editando novamente o zshrc

```
nano ~/.zshrc

### Cole ao final do arquivo esse seguinte código:

export JAVA_HOME=CAMINHO_ANOTADO_COM_SUA_VERSÃO
export ANDROID_HOME=~/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools


Não esqueça de substituir o valor na linha JAVA_HOME pelo caminho que você anotou anteriormente.
```

## Instalando Android Studio

```
Acesse a página do Android Studio e clique no botão Download Android Studio.

Vá até a pasta de download e abra o arquivo dmg.

Nesse momento será apresentada uma janela com a logo do Android Studio e a pasta de Aplicações. Arraste a logo para dentro da pasta e aguarde o processo terminar.

Em seguida, pesquise no Spotlight(comand + espaço) por Android Studio e abra o programa.
```

## Configurando Android Studio

---

- A primeira janela a ser apresentada deve ser perguntando sobre a importação de configurações de outro Android Studio. Selecione a opção Do not import settings e clique em OK.

- Em seguida, o Android Studio começará a carregar. Em algum ponto do carregamento, será apresentada uma janela sobre compartilhamento de dados anônimos com a Google. Essa opção é pessoal, escolha o que preferir.

- Após o carregamento terminar, deve aparecer uma página de Welcome. Clique em Next.

- Na sequência, será pedido o tipo de instalação. Escolha a opção **Custom** e clique em Next.

- Nesse momento, será pedido para escolher a localização do pacote JDK instalado. Clique em ⬇ e verifique se a opção JAVA_HOME está apontando para a JDK 11. Se sim, escolha e Clique em Next. Caso contrário, clique no no botão ... e escolha a JDK 11 (você pode inclusive utilizar o caminho anotado no passo anterior para te ajudar).

  ![](https://react-native.rocketseat.dev/images/android/macos/jdk.png)

- Em seguida, será perguntado sobre qual tema será utilizado. Escolha o que preferir e clique em Next

- Chegamos na etapa mais importante do processo, a instalação da SDK. A janela apresentará algumas opções, marque todas.

  ![](https://react-native.rocketseat.dev/images/android/macos/sdk.png)

- Um fator essencial nessa etapa é o caminho de instalação da SDK. Utilize a pasta que você criou na seção Preparativos para o Android Studio (Ex.: ~/Android/Sdk). Não utilize espaços ou caracteres especiais pois causará erros mais para frente e o próprio Android Studio alerta se o seu caminho não está bom.

- Se tudo estiver correto, clique em Next.

- Na sequência, temos uma janela perguntando sobre a quantidade de RAM que será disponibilizada para que o HAXM utilize. Essa etapa não irá aparecer para todos pois nem todo computador é compatível com esse recurso. Deixe o recomendado pelo programa e clique em Next.

- Em seguida, será apresentada uma janela com um resumo de todas as opções escolhidas até aqui. Verifique se está tudo certo, principalmente os caminhos da SDK e do JDK. Clique em Finish.

- Será realizada a instalação das configurações selecionadas. Quando o programa terminar, clique em Finish.

- Por fim, crie seu emulador e após concluído, Rode o emulador e em seu terminal, digite: **adb devices**

- Deve aparecer uma lista com os dispositivos android conectados e o nome do seu emulador com o status device.

---

## Alterando as Permissões de Aplicativo

```
- Selecione o menu Apple 🍎️ (no canto superior esquerdo da sua tela).
- Abra Preferências do sistema ▸ Segurança e privacidade.
- Selecione a guia Privacidade.
- Selecione Acesso total ao disco e depois clique no ícone de cadeado (Canto inferior esquerdo).
- Habilite as opções de: Terminal, VsCODE e watchman.
- Caso alguma dessas opções não apareçam, selecione o botão de (+), clique em Aplicativos e selecione o aplicativo que está faltando.

```

## Habilitando sua chave SSH

- 1 Digite em seu terminal: ssh-keygen
- 2 Deixe o campo em branco, teclando apenas 'enter' para seguir.
- 3 Repita o segundo passo nas próximas 2 opções.
- 4 cat ~/.ssh/id_rsa.pub | xclip -sel clip (copiando valor da chave).
- 5 Abra seu bitbucket com as informações fornecidas em seu email.
- 6 No canto inferior esquerdo selecione o ultimo ícone ("Your profile and settings") -> "Personal settings" -> "SSH Keys" -> "Add key".
- 7 Crie uma label de sua preferência. Ex: "Mac Nexti".
- 8 Cole sua chave copiada no 3º passo em 'key'.
- 9 Finalize clicando em "Add key".

## Clonando repositório

- Em seu bitbucket selecione "Repositories" -> "nexti-app" -> "clone" -> Certifique-se que está em modo 'SSH' -> copie e clone para sua pasta de desenvolvimento

## Instalando dependências do projeto

```
cd nexti-app

yarn install
```

## Rodando Aplicação

```
(Startando o metro bundler)

yarn start
```

### Android

```
yarn android:qa
```

### IOS

```
cd ios

pod install

caso ocorra erro no processo de pod install, verifique se esse comando resolve: 

sudo pod install --allow-root

cd ..

yarn ios:qa
```
